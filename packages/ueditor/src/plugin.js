import E from 'wangeditor'
const { BtnMenu } = E
export default (safe) => {
  return class Plugin extends BtnMenu {
    constructor(editor) {
      const $elem = E.$(
        `<div class="w-e-menu" data-title="源代码">
              <span class="wangEditor_html" >
              Html
              </span>
            </div>`
      )
      super($elem, editor)
    }
    clickHandler () {
      safe.show = true;
    }
    tryChangeActive () {
      this.active()
    }
  }
}